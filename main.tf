resource "aws_instance" "my-instance" {
	ami = "ami-0d5eff06f840b45e9"
	instance_type = "t2.micro"
	key_name = "Prod"
    subnet_id = "subnet-fa1917b0"
    vpc_security_group_ids = ["sg-0bf8f3e04deaf9eea"]
	#user_data = "file("install.sh")"
	user_data = "${file("install.sh")}"
	tags = {
		Name = "Terraform"	
		
	}
}

variable "EBS_VOLUMES" {
  default = {
    "/dev/sdh" = {
      name = "helloWorld1"
      size        = 10
      type        = "gp3"
      snapshot_id = null

    }
  }

}

output "EBS_VOLUMES_var" {
  value = var.EBS_VOLUMES
}

resource "aws_ebs_volume" "example" {
  for_each          = var.EBS_VOLUMES
  availability_zone = "us-east-1a"
  size              = each.value.size

  tags = {
    Name = each.value.name
  }
}

output "EBS_VOLUMES_res" {
  value = aws_ebs_volume.example
}


resource "aws_volume_attachment" "ebs_att" {
  for_each    = aws_ebs_volume.example
  device_name = each.key
  volume_id   = each.value.id
  instance_id = aws_instance.my-instance.id
}

